/* keystack.c - expose unprotected key in memory; ("find key in the haystack")
 * @author mtreece
 * @date 2020-03-14
 *
 *  Copyright (C) 2020  M. Tyler Reece
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>

#include "pmparser/pmparser.h"

#include "common.h"

static inline bool region_filter(const procmaps_struct *pm)
{
	bool allowed = false;

	/* known issue: http://lkml.iu.edu/hypermail/linux/kernel/1503.1/03733.html */
	if (0 == strcmp(pm->pathname, "[vvar]")) {
		return false;
	}

	/* the key likely lives in the [heap], the [stack], or possibly even an
	 * anonymous mapping (up to the malloc implementation), so let's focus
	 * our efforts on those regions
	 */
	allowed = allowed || (0 == strcmp(pm->pathname, "[heap]"));
	allowed = allowed || (0 == strcmp(pm->pathname, "[stack]"));
	allowed = allowed || (0 == strcmp(pm->pathname, "")); /* anon */

	return allowed;
}

static bool read_stdin(void **data, size_t *len)
{
	const size_t BSZ = 4096; /* block size */
	int ch;

	*data = NULL;
	for (*len = 0; EOF != (ch = getchar()); ++*len) {
		if (!(*len % BSZ) && (!(*data = realloc(*data, *len + BSZ)))) {
			err("out of memory\n");
			return false;
		}
		((uint8_t *)*data)[*len] = ch;
	}

	return true;
}

static bool read_iv(const char *ivhex, void **data, size_t *len)
{
	int res;
	void *d;

	/* strlen(ivhex) should be 2x what we need; that's OK */
	if (!(d = *data = malloc(strlen(ivhex)))) {
		err("out of memory\n");
		return false;
	}

	for (*len = 0, res = 1; *ivhex && res == 1; ivhex += 2, ++d) {
		*len += (res = sscanf(ivhex, "%02x", (unsigned*) d));
	}

	return true;
}

static int pid_memfd(int pid, int *memfd)
{
	/* until PIDs exceed 118 decimal digits, 128 should be fine */
	char path[128];

	if (((int)sizeof(path)) <= snprintf(path, sizeof(path), "/proc/%d/mem",
	                                    pid)) {
		err("error on snprintf\n");
		return -EFBIG;
	}

	if (0 > (*memfd = open(path, O_RDONLY))) {
		err("error on open: %d\n", errno);
		return -errno;
	}

	return 0;
}

static int keysearch(int memfd, uint8_t **key, size_t *keylen,
                     const void *start, unsigned long addrlen,
                     gnutls_cipher_algorithm_t cipher, const void *iv,
                     size_t ivlen, const void *ctext, size_t ctextlen)
{
	int res = 0;
	bool found = false;
	gnutls_cipher_hd_t handle = NULL;
	gnutls_datum_t kd = {0}, ivd = {0}; /* key, iv datum */
	uint8_t *data = NULL, *haystack;
	unsigned long haylen = addrlen;
	void *ptext = NULL;

	if (!(data = haystack = malloc(addrlen))) {
		err("out of memory\n");
		goto cleanup;
	}

	/* (sizeof ptext) <= (sizeof ctext) == ctextlen */
	if (!(ptext = malloc(ctextlen))) {
		err("out of memory\n");
		goto cleanup;
	}

	/* sadly, /proc/.../mem doesn't implement mmap, which ruins a
	 * non-negligible cool factor in this demo ...
	 */
	if ((off_t)start != lseek(memfd, (off_t)start, SEEK_SET)) {
		err("lseek: %d\n", errno);
		goto cleanup;
	}

	if (((ssize_t)addrlen) != read(memfd, haystack, addrlen)) {
		err("read: %d\n", errno);
		goto cleanup;
	}

	/* cast away the const; I pinky promise we won't change it */
	ivd.data = (void *) iv;
	ivd.size = ivlen;

	kd.size = gnutls_cipher_get_key_size(cipher);

	for (; !found && kd.size < haylen; ++haystack, --haylen) {
		kd.data = haystack;

		if ((res = gnutls_cipher_init(&handle, cipher, &kd, &ivd))) {
			err("gnutls_cipher_init: %d\n", res);
			goto cleanup;
		}

		if ((res = gnutls_cipher_decrypt2(handle, ctext, ctextlen,
		                                  ptext, ctextlen))) {
			err("gnutls_cipher_encrypt: %d\n", res);
			goto cleanup;
		}

		/* TODO: further auto confirm key w/ MD5? */
		if (0 == memcmp(HEADER, ptext, HEADERLEN)) {
			if (!(*key = malloc(kd.size))) {
				err("out of memory\n");
				goto cleanup;
			}
			*keylen = kd.size;
			memcpy(*key, kd.data, kd.size);
			found = true;
		}

		gnutls_cipher_deinit(handle); handle = NULL;
	}

cleanup:
	if (handle) {
		gnutls_cipher_deinit(handle); handle = NULL;
	}

	free(data);
	free(ptext);

	return found ? 0 : 1;
}

int main(int argc, char *argv[])
{
	int res = -1;
	int pid = -1;
	int memfd = -1;
	bool found = false;
	int opt;
	char *ivhex = NULL;
	void *iv = NULL;
	size_t ivlen;
	void *ctext = NULL;
	size_t ctextlen;
	uint8_t *key = NULL;
	size_t keylen;
	procmaps_struct *pm;
	procmaps_iterator *maps = NULL;

	while ((opt = getopt(argc, argv, "I:p:")) != -1) {
		switch (opt) {
		case 'p':
			pid = atoi(optarg);
			break;
		case 'I':
			ivhex = optarg;
			break;
		default: /* '?' */
			fprintf(stderr,
			        "syntax: %s <-I IV_HEX_STR> "
			        "<-p $(pgrep badsecrets)> < encrypted_data\n",
			        argv[0]);
			return EXIT_FAILURE;
		}
	}

	if (pid < 0 || !ivhex) {
		err("UTSL!\n");
		goto cleanup;
	}

	if (!read_iv(ivhex, &iv, &ivlen)) {
		err("unable to read IV from args\n");
		goto cleanup;
	}

	/* admittedly, we only need the first block, but we'll consume all the
	 * data just in case we want to expand this functionality to later do
	 * something more advanced (e.g. decrypt all data and/or compute md5)
	 */
	if (!read_stdin(&ctext, &ctextlen)) {
		err("unable to read ctext from stdin\n");
		goto cleanup;
	}

	if ((res = pid_memfd(pid, &memfd))) {
		err("unable to get memory fd: %d\n", res);
		goto cleanup;
	}

	if (!(maps = pmparser_parse(pid))) {
		err("failed to parse proc maps of pid %d\n", pid);
		goto cleanup;
	}

	while (!found && (pm = pmparser_next(maps))) {
		if (!region_filter(pm)) {
			continue;
		}

		switch ((res = keysearch(memfd, &key, &keylen, pm->addr_start,
		                         pm->length, CIPHER, iv, ivlen, ctext,
		                         ctextlen))) {
		case 0: /* successful search and found; let's break */
			found = true;
			break;
		case -EACCES: /* permission denied; propagate up error */
			goto cleanup;
			break;
		case 1: /* successful search but not found; keep trying... */
			break;
		default:
			err("unhandled keysearch result: %d\n", res);
			break;
		}
	}

	if (!found) {
		err("unable to find key ...\n");
		goto cleanup;
	}

	phex("FOUND-KEY", key, keylen);

cleanup:

	free(key);

	if (maps) {
		/* pmparser_free doesn't free maps itself, so free it, too */
		pmparser_free(maps); free(maps);
	}

	if (0 < memfd) {
		/* best effort */
		(void) close(memfd);
	}

	free(ctext);
	free(iv);

	/* propagate up selective error cases */
	switch (res) {
	case -EACCES:
		return -res;
	}

	return found ? EXIT_SUCCESS : EXIT_FAILURE;
}
