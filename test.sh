#!/bin/bash
# Copyright (C) 2020  M. Tyler Reece
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

cleanup()
{
	[ -n "$bspid" ] && kill "$bspid"
}
trap cleanup EXIT

coproc BADSECRETS (stdbuf -i0 -o0 -e0 ./badsecrets)
bspid="$!"

# grab all data we can, up to the prompt
data="$(sed '/Press/q' <&${BADSECRETS[0]})"

md5="$(grep -oP '(?<=MD5: ).*' <<< "$data")"
iv="$(grep -oP '(?<=IV: ).*' <<< "$data")"
encrypted="$(grep -oP '(?<=Encrypted: ).*' <<< "$data")"

cat <<EOF

Launched badsecrets (pid $bspid) produced the following data:

          md5: '$md5'
           IV: '$iv'
    encrypted: '$encrypted'

Let's see if we can find the secret key in its memory ...
EOF

key="$(xxd -r -p <<< "$encrypted" | ./keystack -p "$bspid" -I "$iv" | grep -oP '(?<=FOUND-KEY: ).*$' || exit ${PIPESTATUS[1]})"
res="$?"
case "$res" in
	0)
		;;
	13)
		echo "unable to open process memory; consider running as root" >&2
		exit "$res"
		;;
	*)
		echo "failed to find key!" >&2
		exit "$res"
		;;
esac

# plaintext hex
pthex="$(echo "$encrypted" | xxd -r -p | openssl aes-256-cbc -d -nopad -iv "$iv" -K "$key" | xxd -p)"

cat <<EOF
Discovered key '$key'!
Decrypted data:
$(xxd -r -p <<< "$pthex" | hexdump -C | sed 's,^,    ,')
MD5 of decrypted data: $(xxd -r -p <<< "$pthex" | md5sum | awk '{print $1}')
EOF

if [ x"$(xxd -r -p <<< "$pthex" | md5sum | awk '{print $1}')" == x"$md5" ]; then
	echo "Decryption was successful!"
else
	echo "Decryption FAILED!"
	exit 1
fi
