/* common.h - common between badsecrets, keystack
 * @author mtreece
 * @date 2020-03-22
 *
 *  Copyright (C) 2020  M. Tyler Reece
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#if !defined(_COMMON_H)
#define _COMMON_H

#define err(fmt, ...) \
do { \
	fprintf(stderr, "%s:%u- ERROR: " fmt, __FILE__, __LINE__, \
	        ##__VA_ARGS__); \
} while (0)

#define CIPHER GNUTLS_CIPHER_AES_256_CBC

#define HEADER "SECRET"
#define HEADERLEN (sizeof(HEADER) - 1 /* null byte */)

void phex(const char *what, const void *data, size_t len);

#endif /* _COMMON_H */
