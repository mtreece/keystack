## keystack

Keystack is a "demo" that illustrates the importance of protecting sensitive
key material, even in process-isolated memory. Its name is inspired by the
concept, "finding the needle in a haystack".

### details

The "badsecrets" program will generate a large haystack of random data, then
select a random offset into that buffer for the key. It will generate a random
plaintext, overlap the start of it with a well-known header (ASCII "SECRET"),
then encrypt the plaintext with AES-256-CBC. Finally, it will emit (hex string)
the encrypted ciphertext, the IV, and an MD5 of the plaintext (just a tease).

The "keystack" program will accept the following inputs:

```bash
./keystack -p ${pid} -I ${iv_hex} < ${encrypted_file}
```

... where `${pid}` is the PID of badsecrets, `${iv_hex}` is the IV hex string,
and `${encrypted_file}` is a file containing the _binary_ encrypted data.

To avoid intermediate files, it may be simpler to run like so:

```bash
xxd -r -p <<< ${encrypted_hex} | ./keystack -p ${pid} -I ${iv_hex}
```

Note: your OS is likely configured to help _protect_ against this very attack;
if you receive an error message like, "unable to get memory fd", you will need
to either run keystack as root, or modify `kernel/yama/ptrace_scope` to allow
the operation to continue. (It's likely safer to just temporarily run keystack
as root, rather than alter system-wide security).

### quick example

Glanced at the "complete example" below, but ain't nobody got time for that?
Here's what I've been doing to develop; it's a lot quicker:

```
$ sudo ./test.sh

Launched badsecrets (pid 12345) produced the following data:

          md5: 'e6405ad3eacb6a5020943b088be4de4f'
           IV: '71b6ca4436be1e4da107cdb0baeadd4c'
    encrypted: '44e2ac59c25228261cf64d3e8e65082e8a53ffabc0ae9b6329571b4518fafc3343ca84e65246905da6b0786d293878dc'

Let's see if we can find the secret key in its memory ...
Discovered key '912b4a951b51417a17c948309aa8ed908d693eb3ce5c094259293c117d2dcac4'!
Decrypted data:
    00000000  53 45 43 52 45 54 98 37  76 bd fb 24 96 79 73 48  |SECRET.7v..$.ysH|
    00000010  f1 75 c7 10 cb bc bd 42  95 4e 0b 91 18 95 aa c5  |.u.....B.N......|
    00000020  d6 d3 55 ee 31 b2 a1 99  d7 cf 09 3d 15 29 de 9f  |..U.1......=.)..|
    00000030
MD5 of decrypted data: e6405ad3eacb6a5020943b088be4de4f
Decryption was successful!
```

### complete example

You will need two terminals. In the first, run badsecrets:

```
$ ./badsecrets
MD5: ef3506e45bd4b4a85e37116009137939
IV: bb385e328e11b5747bbad6f475945e00
Encrypted: 39e6ac09a1a2782563812f0733074c587e0082b4f58718fad30729235d0c7750c044a0ba44bd9446c2987bf9a1e53738

My PID is 12345
Press ENTER to exit...
```

Now, (changing values as appropriate), run in the 2nd terminal:

```
$ xxd -r -p <<< 39e6ac09a1a2782563812f0733074c587e0082b4f58718fad30729235d0c7750c044a0ba44bd9446c2987bf9a1e53738 | sudo ./keystack -p 12345 -I bb385e328e11b5747bbad6f475945e00
FOUND-KEY: be71de9b33c02a986476648285d1a265d1a7cb4e6d00e5d5c1a0ecb10b04476f
```

To confirm that the FOUND-KEY is really the key, we can run:

```
$ xxd -r -p <<< 39e6ac09a1a2782563812f0733074c587e0082b4f58718fad30729235d0c7750c044a0ba44bd9446c2987bf9a1e53738 | openssl aes-256-cbc -iv bb385e328e11b5747bbad6f475945e00 -K be71de9b33c02a986476648285d1a265d1a7cb4e6d00e5d5c1a0ecb10b04476f -d -nopad | hexdump -C
00000000  53 45 43 52 45 54 65 26  7f f1 7c 03 75 73 23 d5  |SECRETe&..|.us#.|
00000010  ed 5f bb 21 69 32 e2 1d  69 6a 31 48 92 f6 5c b1  |._.!i2..ij1H..\.|
00000020  04 1a 96 b5 cb 15 3b e1  6e 40 63 b6 50 a9 54 d6  |......;.n@c.P.T.|
00000030

$ xxd -r -p <<< 39e6ac09a1a2782563812f0733074c587e0082b4f58718fad30729235d0c7750c044a0ba44bd9446c2987bf9a1e53738 | openssl aes-256-cbc -iv bb385e328e11b5747bbad6f475945e00 -K be71de9b33c02a986476648285d1a265d1a7cb4e6d00e5d5c1a0ecb10b04476f -d -nopad | md5sum
ef3506e45bd4b4a85e37116009137939  -
```

Note the decrypted hex shows the familiar "SECRET" header, and the MD5 matches
what badsecrets claimed.

### building

Building the project should be as simple as running "make" in the top-level
directory. The project has a dependency on gnutls, so you may need to install
the gnutls library and its header files (development files). Consult your OS's
package manager for how to install these dependencies.

### license

Copyright (C) 2020  M. Tyler Reece

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
