/* badsecrets.c - generate random data, encrypt it w/ unprotected key
 * @author mtreece
 * @date 2020-03-14
 *
 *  Copyright (C) 2020  M. Tyler Reece
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/random.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <gnutls/gnutls.h>
#include <gnutls/crypto.h>

#include "common.h"

#define MiB (1024 * 1024)

/* how big should the haystack be for us to brute force find the key? */
#define HAYSTACKLEN (1 * MiB)

/* ensures we "keep going" if getrandom returns insufficient length */
static ssize_t getrandom_(void *buf, size_t buflen, unsigned int flags)
{
	ssize_t res = 0;
	ssize_t ires; /* intermediate res */

	do {
		/* XXX: consider checking errno & cont'ing on some cases */
		ires = getrandom(buf, buflen, flags);
		if (ires < 0) {
			perror("getrandom");
			return ires;
		}
		res += ires;
		buf += ires;
		buflen -= ires;
	} while (buflen);

	return res;
}

static int allokey(size_t haystacklen, void **base, unsigned char **data,
                   unsigned int size)
{
	size_t offset;

	if (haystacklen < size) {
		err("haystacklen %zu < needed %u\n", haystacklen, size);
		return -1;
	}

	if (!(*base = malloc(haystacklen))) {
		err("Out of mem\n");
		return -ENOMEM;
	}

	if (((ssize_t)haystacklen) != getrandom_(*base, haystacklen, 0)) {
		err("Error calling getrandom_ for base\n");
		return -1;
	}

	if (((size_t)sizeof(offset)) != getrandom_(&offset, sizeof(offset), 0)) {
		err("Error calling getrandom_ for offset\n");
		return -1;
	}

	offset %= (haystacklen - size) + 1;

	*data = *base + offset;

	return 0;
}

int main(int argc, char *argv[])
{
	int res = 0;
	gnutls_cipher_hd_t handle = NULL;
	gnutls_digest_algorithm_t digest;
	void *dr = NULL; /* digest result */
	size_t dlen8; /* digest length */
	gnutls_datum_t td = {0}; /* text datum */
	gnutls_datum_t key = {0}, iv = {0};
	void *keybase = NULL;

	key.size = gnutls_cipher_get_key_size(CIPHER);
	iv.size = gnutls_cipher_get_iv_size(CIPHER);

	digest = GNUTLS_DIG_MD5;
	dlen8 = gnutls_hash_get_len(digest);

	/* any multiple of the block size will do */
	td.size = 3 * iv.size;

	if (argc != 1 && argc != 2) {
		fprintf(stderr, "syntax: %s [output file]\n", argv[0]);
		goto exit;
	}

#define allocate_random(buffer, len) \
do { \
	if (!((buffer) = malloc((len)))) { \
		res = -ENOMEM; \
		err("No memory to allocate %s!\n", #buffer); \
		goto exit; \
	} \
	if (((ssize_t)(len)) != getrandom_((buffer), (len), 0)) { \
		res = -errno; \
		err("Error calling getrandom_ for %s!\n", #buffer); \
		goto exit; \
	} \
} while (0)

	allocate_random(td.data, td.size);
	allocate_random(iv.data, iv.size);
	allocate_random(dr, dlen8); /* random, just for ease */

#undef allocate_random

	/* use special procedures to generate & hide the key */
	if (allokey(HAYSTACKLEN, &keybase, &key.data, key.size)) {
		res = -1;
		err("Error allocating key!\n");
		goto exit;
	}

	/* overlay well-known header to ease brute force recognition */
	memcpy(td.data, HEADER, HEADERLEN);

	/* tease the user by showing them an MD5SUM of the plaintext */
	if ((res = gnutls_fingerprint(digest, &td, dr, &dlen8))) {
		err("gnutls_fingerprint: %d\n", res);
		goto exit;
	}

	phex("MD5", dr, dlen8);
	phex("IV", iv.data, iv.size);

	if ((res = gnutls_cipher_init(&handle, CIPHER, &key, &iv))) {
		err("gnutls_cipher_init: %d\n", res);
		goto exit;
	}

	if ((res = gnutls_cipher_encrypt(handle, td.data, td.size))) {
		err("gnutls_cipher_encrypt: %d\n", res);
		goto exit;
	}

	/* the handle likely retains a copy of the key, so let's clean that up;
	 * otherwise, it can defeat the "hiding in a haystack" by having a copy
	 * laying at a fixed memory offset, in addition to our true key addr
	 */
	if (handle) {
		gnutls_cipher_deinit(handle); handle = NULL;
	}

	phex("Encrypted", td.data, td.size);

	if (argc == 2) {
		int fd;
		if (-1 == (fd = open(argv[1],
		                     O_WRONLY | O_CREAT | O_TRUNC,
		                     S_IRUSR | S_IWUSR))) {
			perror("open");
			goto exit;
		}
		if (td.size != write(fd, td.data, td.size)) {
			perror("write");
			(void) close(fd);
			goto exit;
		}
		(void) close(fd);
	}

	printf("\nMy PID is %d\n", getpid());
	printf("Press ENTER to exit...\n");
	(void) getchar();

exit:
	if (handle) {
		gnutls_cipher_deinit(handle);
	}

	free(td.data);
	free(keybase);
	free(iv.data);
	free(dr);

	return res ? EXIT_FAILURE : EXIT_SUCCESS;
}
